---
layout: post
title:  সবকিছু জেনে ফেলা?
---
সেদিন একজন ইনবক্সে প্রশ্নগুলো করেছেন। সেগুলোর উত্তর দিচ্ছি।
<strong>১) আপনি কী মনে করেন, আপনি একজন মুক্তমনা হয়ে গেছেন?</strong>


&gt; না, আমি তা মনে করি না। মুক্তচিন্তার মানুষ হওয়াটা হয়ে যাওয়া বা না হওয়ার মত বিষয় নয়। এটা ক্রমশ নিজেকে শুধরে নেয়ার প্রক্রিয়া। একটা সিঁড়ির মত, ধাপে ধাপে উপরের দিকে উঠতে হয়। এবং এই সিঁড়ির কোন শেষ নেই। মৃত্যু পর্যন্ত চেষ্টা করে যেতে হবে যেন একজন যুক্তিবাদী, মননশীল, মানবিক, মুক্তচিন্তার মানুষ হওয়া যায়।
আমি আজকে যা জানি, কাল যদি যুক্তি দিয়ে বুঝতে পারি গতকালের ভাবনাটা ভুল ছিল, বিনয়ের সাথে নিজেকে শুধরে নেয়ার চেষ্টা করবো। কারণ ভুল করে করেই আমি শিখবো। বিজ্ঞানও এভাবেই কাজ করে। নতুন তথ্য প্রমাণ গবেষণাতে কাল যদি দেখা যায়, পুরনো ধারণাটি মিথ্যা ছিল, সাথে সাথে সেটা শুধরে নিতে বিজ্ঞান দ্বিধা করে না। ভুল স্বীকার করে নিজেকে সংশোধন করে নেয়া খুবই জরুরি। বিজ্ঞানের অনেক বড় বড় গবেষণাকে অনেক ছোটখাটো গবেষক ভুল প্রমাণ করে নোবেল পুরষ্কার জিতে নিয়েছেন। এমনকি, কোথাকার কোন কলিমুদ্দীন রহিমুদ্দীন যদি কাল নিউটনের সূত্রকে ভুল প্রমাণ করে দেয়, নির্দ্বিধায় সে নোবেল পাবে এবং তামাম দুনিয়ার বিজ্ঞানীরা তাকে মাথায় তুলে নাচবে।

<img class="size-medium wp-image-546 alignleft" src="https://gitlab.com/asifmoh/nastikya/raw/master/images/egyptIslamists-300x218.jpg" alt="" width="300" height="218" />

কোথাকার কোন রহিমুদ্দীন কলিমুদ্দীন বিজ্ঞানের ধারণাটাই যদি পাল্টে দেয়, তাতে বিজ্ঞানের এতটুকু অসম্মান হবে না। বরঞ্চ মানুষ সবসময় তাকে সম্মান করবে যে এরকম আবিষ্কার করবে। নতুন কোন তত্ত্ব দেবে। পৃথিবীকে নতুন পথ দেখাবে।
কিন্তু ধর্ম কাজ করে ঠিক উলটো পদ্ধতিতে। ধর্ম কিছু একটা দাবী করে, এবং প্রাপ্ত তথ্য প্রমাণ যুক্তির সাথে সেটা সাংঘর্ষিক হলে নিজের ইগো রক্ষা করতে নানান ধানাই পানাই করতে থাকে। মাঝে মাঝে গলাও কাটে। মাঝে মাঝে বলতে শুরু করে, তুমি অমুক বিষয়টি বোঝো নি। ব্যাখ্যা পড়তে হবে। পরিপ্রেক্ষিত জানতে হবে। প্রেক্ষাপট বুঝতে হবে। ট্যাঁ ফ্যাঁ। য়্যাঁ য়্যাঁ য়্যাঁ য়্যাঁ। তারপরে যথারীতি নারায়ে তাকবীর বলে চাপাতির কোপ।

যেমন ধরুন,
" আবু যর গেফারি (রা.) একদিন রাসুলুল্লাহ (সাঃ) এর সাথে সূর্যাস্তের সময় মসজিদে উপস্থিত ছিলেন। রাসুলুল্লাহ (সাঃ) বললেন আবু যর, সূর্য কোথায় অস্ত যায় জানো? আবু যর বললেন, আল্লাহ এবং তাঁর রাসুলই ভাল জানেন। তখন রাসুলুল্লাহ (সাঃ) বললেন, সূর্য চলতে চলতে আরশের নিচে পৌঁছে সেজদা করে। হযরত আব্দুল্লাহ ইবনে ওমর থেকেও এই সম্বন্ধে হাদীস বর্ণিত হয়েছে যে, প্রত্যেক সূর্য আরশের নিচে পৌঁছে সেজদা এবং নতুন পরিভ্রমণের অনুমতি প্রার্থনা করে। অনুমতি লাভ করে নতুন পরিভ্রমণ শুরু করে। (তফসীর মাআরেফুল কোরআন, পৃষ্ঠা-১১৩৩)"

আবার ধরুন,
“এবং তিনি হচ্ছেন সেই সত্ত্বা যিনি দুই সমুদ্রকে (একত্রে পাশাপাশি) প্রবাহিত করেছেন, একটির পানি মিষ্টি ও সুপেয় এবং অপরটির পানি লোনা ও বিস্বাদ; উভয়ের মধ্যে তিনি রেখেছেন একটি অন্তরায়,একটি দুর্ভেদ্য দেয়াল (যাতে তারা মিলে যেতে না পারে)।” (২৫:৫৩)

উপরের হাদিস এবং কোরানের আয়াত দুটো যে কোন চিন্তাশীল মানুষ পড়লেই বুঝবে, এখানে বড় ধরণের সমস্যা আছে। সূর্য কখনো আরশের নিচে পৌঁছে সেজদা করে না। প্রাচীনকালে রাতে সূর্য কোথায় যায় এটা অনেক মানুষই ভেবে পেতো না। সেই সময়ে যে পৃথিবীর অপরপ্রান্তের মানুষ সূর্য দেখছে, সেটা জ্ঞানের অভাবের কারণে তাদের বোধগম্য হতো না। এসময়ে নানা উপকথা কিংবা গল্প প্রচলিত ছিল যে, এই সময়ে সূর্য বিশ্রাম নেয়। কিংবা দেবতাদের সাথে আড্ডা দিতে যায়। ইত্যাদি। হাদিসের কথাটিও একই রকম।

আর সুন্দরবনে যারা বসবাস করেন, তারা ভালভাবেই জানেন যে, জোয়ার ভাঁটার কারণে মাঝে মাঝেই সুন্দরবনের নদীতে লবণাক্ত পানি চলে আসে। মিস্টি পানি আর নোনা পানির মধ্যে যেই দুর্ভেদ্য দেয়ালের কথা বলা, সেরকম কোন দেয়াল নেই। লবণাক্ত পানি ক্রমশই মিঠা পানিকে লবণাক্ত করে ফেলে। আপনি চাইলে বাসাতে পরীক্ষাটি করতে পারেন। লবণাক্ত এবং মিঠা পানি দিয়ে। আপনার বাসাতে যা হবে, সমুদ্র এবং নদীর মোহনাতেও কিংবা দুই সমুদ্রের মাঝে অনেকটা তাই হয়। নদীর স্রোতের কারণে নদীর পানির লবণাক্ত হওয়াটা স্বাভাবিকভাবে বোঝা যায় না। তবে নদীতে স্রোত কমে গেলে সমুদ্রের নোনা পানি প্রায়ই নদীতে চলে আসে। দুর্ভেদ্য দেয়াল তখন আর তাকে থামাতে পারে না। আর দুই সমুদ্রের মাঝেও ক্রমাগত মিশ্রণ হতে থাকে। কতটা মিশ্রণ হবে কীভাবে হবে তা নির্ভর করে সুমুদ্রের পানির বৈশিষ্ট্যের ওপর। স্রোতের ওপর। কোন অবস্থাতেই মাঝে কোন দুর্ভেদ্য দেয়াল থাকে না। প্রয়োজনে দুই সমুদ্রের মাঝে বা একপাশে কোন বিষাক্ত পদার্থ বা তেজস্ক্রিয় পদার্থ ফেলে দেখতে পারেন। ঐ পাশের পানি তেজস্ক্রিয় হয় কিনা। দুর্ভেদ্য দেয়াল থাকলে তো ঐ পাশের পানি পরিষ্কারই থাকবে, নাকি? মাঝে তো দুর্ভেদ্য দেয়াল রয়েছেই, ভয় কী?

এরকম হাজার হাজার উদাহরণ দেয়া সম্ভব। ধর্মগ্রন্থের এই ভুলগুলো ধরার কারণে ধার্মিকরা জবাই করবে, বিজ্ঞানের এরকম ভুল ধরে দিলে তারা নোবেল পুরষ্কার দেবে। আপনাকে মহান বিজ্ঞানী বলে ডাকবে। পার্থক্য তো আছেই।


<strong>২) আপনি কি মহান বিজ্ঞানী হয়ে গেছেন? আপনি কি সব জেনে ফেলেছেন? কাল যদি জানা যায় আল্লাহ আছে, তখন?</strong>


&gt; না আমি সব জেনে ফেলি নি। কেউই সব জানতে পারে না। তবে জানার চেষ্টা করছি, অনবরত। পৃথিবীর সকল জিজ্ঞাসু মানুষই জানার চেষ্টা করে যাচ্ছে। কিন্তু এইটুকু বুঝি, ১৫০০ বছর আগে এক আরব বর্বর যুদ্ধবাজ লম্পট লোক গুহায় বসেও সব জেনে ফেলে নি। আর সেরকম দাবী করলেও, সেসব হাস্যকর। অতীতের মানুষের জ্ঞান আরও কম ছিল, তাদের কাছে তথ্য প্রমাণ আরও অনেক কম ছিল। আপনি নিজেই ভেবে দেখেন, আপনার পরদাদা বা নানী হয়তো এটাও জানতো না যে, সুর্য পৃথিবীর চারদিকে ঘোরে নাকি পৃথিবী ঘোরে; অথবা পৃথিবী গোলাকার কিনা। আপনি তা জানেন। তারো আগের দিনের মানুষ আরও কম জানতো নিশ্চিতভাবেই। দিনে দিনে আমরা আরও বেশি কিছু জানছি। আজ আমরা বিগ ব্যাঙ থিওরি নিয়ে আলাপ করছি। মাল্টি ভার্স থিওরি নিয়ে কথা বলছি।
আমি সব জেনে ফেলি নি অবশ্যই। তবে শেওড়া গাছের পেত্নী যে মানুষের কল্পনা, তা বোঝার জন্য সব জেনে যাওয়ার প্রয়োজন নেই। সামান্য কমনসেন্স এবং যৌক্তিক চিন্তা থাকলেই তা বোঝা যায়।
এখন কেউ যদি বলে, যেহেতু আমি সব জ্ঞান পেয়ে যাই নি, তাই শেওড়া গাছের পেত্নীও থাকতে পারে, সেটা হাস্যকর দাবী।
কারণ "সব জেনে ফেলো নি তাই অমুক বিষয়টা সত্যি" এই যুক্তিকে ধরে নিলে পৃথিবীর সমস্ত দাবীই সত্য বলে ধরে নিতে হয়। এই দাবীটা ধোপে টেকে না। তাহলে আমিও পালটা বলতে পারি, তুমিও তো সব জেনে যাও নি, যেহেতু জেনে যাও নি, কাল যদি প্রমাণিত হয় যে, অমুক বিষয়টা মিথ্যা, তখন?

আমি যদি বলি, বিষ্ঠায় আসলে সুগন্ধ পাওয়া যায়। কিন্তু তুমি আজকে তা বুঝতে পারছো না, কাল যখন তোমার জ্ঞান হবে তখন বুঝবে!

বা যদি বলি, কলিমুদ্দিনের ছাগলটা আসলে আপনার চাইতেও বুদ্ধিমান জীব। আপনার জ্ঞান কম তাই জানেন না। কাল বিষয়টা আবিষ্কার হলে জানবেন!

বা যদি বলি, আমার বাসার বেড়ালটা রাতের বেলা আমার সাথে কথা বলে। আপনি কী সব জেনে ফেলেছেন? কাল যদি জানা যায়, বেড়ালটা আসলে গোপনে কথা বলে, তখন?

<strong>কাল কী জানা যাবে, কাল এমনটা হতেও পারে, বা নাও হতে পারে, সেটার ওপর ভিত্তি করে আজকে কোন যৌক্তিক সিদ্ধান্ত নেয়া যায় না। কাল যদি জানা যায় আল্লাহ আছে, বা শেওড়া গাছের পেত্নী আছে, বা আপনার একটা কথা বলা বেড়াল আছে, এমন তথ্য প্রমাণ আপনি দিতে পারেন, তাহলে এখনি সেসব দিতে সমস্যা কী?</strong>
