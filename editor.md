---
layout: pages
title: সম্পাদকের কথা 
---


![আসিফ মহিউদ্দীন](http://www.nastikya.com/wp-content/uploads/2017/01/110997-ld-300x300.jpg)

আসিফ মহিউদ্দীন বাঙলা ব্লগ জগতে একজন আলোচিত এবং সমালোচিত নাম। ইতিপুর্বে বেশ কয়েকটি দেশি এবং আন্তর্জাতিক পুরষ্কারপ্রাপ্ত এই ব্লগার মূলত লেখেন মানবতাবাদ, মৌলবাদ-সাম্প্রদায়িকতা বিরোধী, সাম্রাজ্যবাদ বিরোধী, নারীবাদ, শিক্ষার অধিকার এবং নানাধরণের আলোচিত বিষয় নিয়ে। বাঙলাব্লগস্ফিয়ারে মুক্তবুদ্ধির আন্দোলনের একজন কর্মী হিসেবে এবং সোশ্যাল একটিভিস্ট হিসেবে তিনি ইতিমধ্যে বাঙলা ব্লগারদের মধ্যে বিশেষ গুরুত্বপুর্ণ স্থান করে নিয়েছেন। শুধুমাত্র ব্লগিং এর কারণে বাঙলাদেশে সর্বপ্রথম তিনি পুলিশী নির্যাতনের শিকার হন, একই সাথে ব্লগ-ফেসবুক থেকে আন্দোলনকে রাজপথে নিয়ে যাবার জন্যেও তিনি আলোচিত। বাঙলাব্লগে যদিও তিনি ধর্মবিরোধী লেখার কারণে প্রবল সমালোচিত, একই সাথে বাক-স্বাধীনতার পক্ষে জনমত সৃষ্টিতে তার ভূমিকা অস্বীকার করা যায় না। ইতিমধ্যে তিনি বাঙলাব্লগস্ফিয়ারে মুরতাদ খেতাবে ভুষিত হয়েছেন, এবং ধর্মান্ধতা ও মৌলবাদের বিপক্ষে জনমতও তৈরি করে যাচ্ছেন।

&nbsp;

পুরষ্কারঃ

২০১৫ সালে <a href="http://www.dhakatribune.com/bangladesh/2015/oct/02/asif-mohiuddin-receives-anna-politkovskaya-award">আনা পলিটকোভস্কায়া পুরষ্কার </a>

২০১৪ সালে <a href="https://humanism.org.uk/2014/08/09/bangladeshi-bloggers-asif-mohiuddin-late-ahmed-rajib-win-free-expression-award-world-humanist-congress/">Free Expression Award</a>

২০১২ সালে ডয়েচে ভেলে <a href="http://thebobs.com/english/category/history/?year=history-2012&amp;content=winner">সেরা সোশ্যাল একটিভিস্ট </a>

২০১১ সালে সামহোয়্যার ইন ব্লগ সেরা ব্লগার

আরোঃ <a href="https://en.wikipedia.org/wiki/Asif_Mohiuddin">উইকিপিডিয়া</a>

যাদের দ্বারা অনুপ্রাণিতঃ আরজ আলী মাতুব্বর, হুমায়ুন আজাদ, আহমদ শরীফ, বার্টান্ড রাসেল, সিমোন দ্য বুভোয়া, মিশেল ফুঁকো, জ্যা পল সার্ত্র, ভলতেয়ার, ক্রিস্টোফার হিচেন্স, আয়ান হিরসি আলী, সক্রেটিসএবং গৌতম বুদ্ধ।

&nbsp;

ছবিঘরঃ

<img class="alignnone wp-image-497 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/12108124_912154915498604_8460501011640679441_n.jpg" width="960" height="650" />

<img class="alignnone wp-image-490 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10435720_721068861273878_5353219408613407961_n.jpg" width="960" height="640" />

<img class="alignnone wp-image-496 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/12094800_912179302162832_5674104799137083928_o.jpg" width="795" height="960" />

<img class="alignnone wp-image-491 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11009146_10204707491185546_2440646383339650193_n.jpg" width="960" height="720" />

<img class="alignnone wp-image-492 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11055350_10153002414900698_2950302026224714251_n.jpg" width="960" height="640" />

<img class="alignnone wp-image-493 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11407229_862300723817357_8914832025897757214_n.jpg" width="720" height="960" />

<img class="alignnone wp-image-494 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11425132_458000814373409_8214928108982486183_n.jpg" width="960" height="720" />

<img class="alignnone wp-image-495 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11745774_879971048716991_8309518574445008287_n.jpg" width="960" height="717" />

<img class="alignnone wp-image-498 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/13912866_10155005385418906_5935415882879989672_n.jpg" width="960" height="720" />

<img class="alignnone size-full wp-image-499" src="http://www.nastikya.com/wp-content/uploads/2017/01/44283_485007288213371_68622290_n.jpg" alt="" width="960" height="743" /> <img class="alignnone wp-image-500 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/574461_488577517856348_267224952_n.jpg" width="604" height="372" /> <img class="alignnone wp-image-501 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/1959368_721069447940486_4739270123777360130_n.jpg" width="960" height="544" /> <img class="alignnone wp-image-502 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10300305_717276978319733_6224898525517659255_n.jpg" width="960" height="720" /> <img class="alignnone wp-image-503 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10348225_683221858391912_6258994304200323831_n.jpg" width="900" height="600" /> <img class="alignnone wp-image-504 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10358568_721069234607174_6345719124977298287_n.jpg" width="640" height="427" /> <img class="alignnone wp-image-505 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10405468_721069594607138_6075727979995182679_n.jpg" width="960" height="640" /> <img class="alignnone wp-image-506 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10472782_721069431273821_8842676341088767558_n.jpg" width="960" height="640" /> <img class="alignnone wp-image-507 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10505571_721069017940529_141176709087271840_n.jpg" width="640" height="427" /> <img class="alignnone wp-image-508 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10550854_721069574607140_7230576117008377178_n.jpg" width="640" height="960" /> <img class="alignnone wp-image-509 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10557225_721068994607198_2634871581589693420_n.jpg" width="640" height="427" /> <img class="alignnone wp-image-510 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10561777_717989648248466_8811127681583174257_n.jpg" width="960" height="720" /> <img class="alignnone wp-image-511 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/10561790_721069247940506_293191098942632237_n.jpg" width="960" height="640" /> <img class="alignnone wp-image-512 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11066705_830937496953680_8974182355184694244_n.jpg" width="960" height="720" /> <img class="alignnone wp-image-513 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11088567_830185270362236_440347491859174096_n.jpg" width="960" height="720" />

<img class="alignnone size-full wp-image-517" src="http://www.nastikya.com/wp-content/uploads/2017/01/FB_IMG_1442709439173.jpeg" alt="" width="924" height="616" /><img class="alignnone wp-image-514 size-full" src="http://www.nastikya.com/wp-content/uploads/2017/01/11146275_833432196704210_8128487204970534137_n.jpg" width="960" height="239" />

যোগাযোগঃ admin@nastikya.com
